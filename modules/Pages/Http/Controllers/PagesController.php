<?php

namespace Modules\Pages\Http\Controllers;

use App\Http\Controllers\Controller;
use Modules\Pages\Page;
use Location;

class PagesController extends Controller
{
	public function index()
	{
		$pages = Page::all();
		return view('Page::index', compact('pages'));
	}

	public function view()
	{
		echo Location::getLocation();
	}
}