<h1>{{ trans('Page::pages.pages') }}</h1>

<table border="1">
	<thead>
		<tr>
			<th style="padding: 10px;">{{ trans('Page::pages.title') }}</th>
			<th style="padding: 10px;">{{ trans('Page::pages.body') }}</th>
		</tr>
	</thead>
	<tbody>
		@foreach($pages as $page)
			<tr>
				<td style="padding: 10px;">{{ $page->title }}</td>
				<td style="padding: 10px;">{{ $page->body }}</td>
			</tr>
		@endforeach
	</tbody>
</table>